# Generated by Django 3.1.7 on 2021-03-21 21:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('moment', '0001_initial'),
        ('station', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='TemperatureMeasurement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('max_value', models.FloatField()),
                ('min_value', models.FloatField()),
                ('max_dew_point', models.FloatField()),
                ('min_dew_point', models.FloatField()),
                ('moment_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='temperature', to='moment.moment')),
                ('station_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='temperature', to='station.station')),
            ],
        ),
    ]
