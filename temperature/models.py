from django.db import models
from station.models import Station
from moment.models import Moment


class TemperatureMeasurement(models.Model):
    station = models.ForeignKey(
        Station, on_delete=models.CASCADE, related_name='temperature')
    moment = models.ForeignKey(
        Moment, on_delete=models.CASCADE, related_name='temperature')
    maximum = models.FloatField(null=True)
    minimum = models.FloatField(null=True)
    maximum_dew_point = models.FloatField(null=True)
    minimum_dew_point = models.FloatField(null=True)
    air = models.FloatField(null=True)
    dew_point = models.FloatField(null=True)

    def __str__(self):
        return f'{self.maximum} - {self.minimum}'
