from .views import TemperatureMeasurementView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'temperature', TemperatureMeasurementView)
urlpatterns = router.urls
