from .models import TemperatureMeasurement
from rest_framework import serializers


class TemperatureMeasurementSerializer(serializers.ModelSerializer):
    class Meta:
        model = TemperatureMeasurement
        fields = '__all__'
