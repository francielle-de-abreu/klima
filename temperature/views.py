from rest_framework.mixins import ListModelMixin, CreateModelMixin, RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin
from rest_framework.viewsets import GenericViewSet
from .models import TemperatureMeasurement
from .serializers import TemperatureMeasurementSerializer
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page


class TemperatureMeasurementView(GenericViewSet,
                                 RetrieveModelMixin,
                                 CreateModelMixin,
                                 UpdateModelMixin,
                                 DestroyModelMixin,
                                 ListModelMixin):
    @method_decorator(cache_page(10))
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    queryset = TemperatureMeasurement.objects.all()
    serializer_class = TemperatureMeasurementSerializer
