from django.db import models
from station.models import Station
from moment.models import Moment


class Radiation(models.Model):
    station = models.ForeignKey(
        Station, on_delete=models.CASCADE, related_name='radiation')
    moment = models.ForeignKey(
        Moment, on_delete=models.CASCADE, related_name='radiation')
    global_value = models.FloatField(null=True)

    def __str__(self):
        return f'{self.global_value}'
