from .views import RadiationView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'radiation', RadiationView)
urlpatterns = router.urls
