from django.shortcuts import render
from rest_framework.mixins import ListModelMixin, CreateModelMixin, RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin
from rest_framework.viewsets import GenericViewSet
from .models import Radiation
from .serializers import RadiationSerializer
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page


class RadiationView(GenericViewSet,
                    RetrieveModelMixin,
                    UpdateModelMixin,
                    CreateModelMixin,
                    DestroyModelMixin,
                    ListModelMixin):

    @method_decorator(cache_page(10))
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    queryset = Radiation.objects.all()
    serializer_class = RadiationSerializer
