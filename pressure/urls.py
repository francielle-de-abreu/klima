from .views import PressureMeasurementView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'pressure', PressureMeasurementView)
urlpatterns = router.urls
