from rest_framework import serializers
from .models import PressureMeasurement


class PressureMeasurementSerializer(serializers.ModelSerializer):
    class Meta:
        model = PressureMeasurement
        fields = '__all__'
