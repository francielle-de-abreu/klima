from django.db import models
from station.models import Station
from moment.models import Moment


class PressureMeasurement(models.Model):
    station = models.ForeignKey(
        Station, on_delete=models.CASCADE, related_name='pressure')
    moment = models.ForeignKey(
        Moment, on_delete=models.CASCADE, related_name='pressure')
    at_level = models.FloatField(null=True)
    maximum = models.FloatField(null=True)
    minimum = models.FloatField(null=True)

    def __str__(self):
        return f'{self.at_level}'
