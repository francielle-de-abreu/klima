from .views import ThermalSensationView
from django.urls import path

urlpatterns = [
    path('thermal_sensation/<str:station>/',
         ThermalSensationView.as_view()),
]
