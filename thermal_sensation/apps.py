from django.apps import AppConfig


class ThermalSensationConfig(AppConfig):
    name = 'thermal_sensation'
