
from rest_framework.views import APIView
from station.models import Station
from moment.models import Moment
from wind.models import WindMeasurement
from temperature.models import TemperatureMeasurement
from wind.models import WindMeasurement
from rest_framework.response import Response
import math
from django.shortcuts import get_object_or_404


class ThermalSensationView(APIView):
    def get(self, request, station: str):
        date = request.GET.get("date", None)

        time = request.GET.get("time", None)

        station_curret = get_object_or_404(Station,
                                           code=station)

        moment = get_object_or_404(Moment,
                                   date=date, time=time)

        wind = WindMeasurement.objects.filter(
            station__code=station).filter(moment__date=date, moment__time=time)

        temperature = TemperatureMeasurement.objects.filter(
            station__code=station).filter(moment__date=date, moment__time=time)

        v = float(wind.values()[0]['speed']) * 3.6

        st = 33 + (10 * math.sqrt(v) + 10.45 - v) * \
            (float(temperature.values()[0]['air']) - 33)/22

        respose = {
            "station": station_curret.name,
            "temperature": float(temperature.values()[0]['air']),
            "windspeed": float(wind.values()[0]['speed']),
            "sensation": st
        }

        return Response(respose)
