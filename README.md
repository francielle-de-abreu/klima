# Klima

## Django version:

3.1.6

## The application:

-This application was implemented to Consult data that has been measured (humidity, pressure, temperature, radiation ...). But in addition to viewing the recorded data, it is possible to edit and register to register new measurements.

## URL base:

```
http://127.0.0.1:8000/api/
```

## Basic dependencies development environment

- Docker version 20.10.5
- Docker-compose version 1.28.4

## Build

Please, run:

```
docker-compose up --build
```

## Tests

Please, run:

```
docker-compose run -e TEST=TEST web python manage.py test
```

## Database:

- PostgreSQL

## Library for development

- The libs can be found at requirements.txt

# ENDPOINTS

## CRUD:

### /wind

### /humidity

### /pressure

### /radiation

### /temperature

### /rainfall

### /state

### /moment

- Through these routes it is possible to add new data and read all the registered data, it is also possible to edit and delete, however the PK must be passed as a parameter in the url.

### GET /min-max/<str:agrupamento>/

- Route that returns the minimum and maximum value for each measure among all the values that were recorded:

```
- Route: min-max/region

[
  {
    "CO": {
      "pressure": {
        "max": {
          "value": 1008.1,
          "date": "2005-06-23 17:00:00"
        },
        "min": {
          "value": 866.5,
          "date": "2004-10-03 22:00:00"
        }
      },
    ...

- Route: min-max/state

[
  {
    "DF": {
      "pressure": {
        "max": {
          "value": 1008.1,
          "date": "2005-06-23 17:00:00"
        },
        "min": {
          "value": 866.5,
          "date": "2004-10-03 22:00:00"
        }
      },
      "radiation": {
        "max": {
          "value": 43969.0,
          "date": "2008-09-11 16:00:00"
        },
        "min": {
          "value": 0.0,
          "date": "2020-01-03 16:00:00"
        }
      },...

- Route: min-max/station

[
  {
    "BRASILIA": {
      "pressure": {
        "max": {
          "value": 1008.1,
          "date": "2005-06-23 17:00:00"
        },
        "min": {
          "value": 866.5,
          "date": "2004-10-03 22:00:00"
        }
      },...
```

### GET precipitation/<str:agrupamento>/

- Route that calculates the sum of the precipitations in a determined period.

```
- Route: /precipitation/station/?year=2020

[
  {
    "BRASILIA": {
      "accumulation_in_hour__sum": 1576.6000000000056
    }
  },
  {
    "GOIANIA": {
      "accumulation_in_hour__sum": 1508.8000000000034
    }
  },...

- Route: /precipitation/state/?year=2005&month=7

 {
    "PR": {
      "accumulation_in_hour__sum": 39.60000000000002
    }
  },
  {
    "RR": {
      "accumulation_in_hour__sum": 52.20000000000004
    }
  },....

- Route: /precipitation/region/?year=2012&month=12&day=12

  {
    "S": {
      "accumulation_in_hour__sum": 6.6000000000000005
    }
  },
  {
    "N": {
      "accumulation_in_hour__sum": 11.4
    }
  }....

```

### GET /station-report/<str:code>/<str:kind>

- Route that averages measurements for a given station. The code of the station where the results are to be obtained must be passed.

```
- Route: /station-report/A001/daily

{
  "2021-01-31": {
    "avg_gust": 4.43333333333333,
    "avg_speed": 1.925,
    "avg_max_humidity": 63.5416666666667,
    "avg_min_humidity": 52.7083333333333,
    "avg_relative_humidity": 57.75,
    "avg_temperature_max": 25.3208333333333,
    "avg_temperature_min": 23.3125,
    "avg_temperature_max_dew": 15.675,
    "avg_temperature_min_dew": 13.875,
    "avg_accumulation": 0.0,
    "avg_radiation_global": 2307.85384615385,
    "avg_max_pressure": 886.741666666667,
    "avg_min_pressure": 886.145833333333,
    "avg_at_level_pressure": 886.45
  },
  "2021-01-30": {....

- Route: /station-report/A001/weekly

{
  "2021-01-25": {
    "avg_gust": 4.98154761904762,
    "avg_speed": 2.02023809523809,
    "avg_max_humidity": 69.7559523809524,
    "avg_min_humidity": 59.9404761904762,
    "avg_relative_humidity": 65.1071428571429,
    "avg_temperature_max": 23.4214285714286,
    "avg_temperature_min": 21.5839285714286,
    "avg_temperature_max_dew": 15.725,
    "avg_temperature_min_dew": 14.0244047619048,
    "avg_accumulation": 0.788095238095238,
    "avg_radiation_global": 1953.75161290323,
    "avg_max_pressure": 886.277380952381,
    "avg_min_pressure": 885.740476190476,
    "avg_at_level_pressure": 886.005357142857
  },
  "2021-01-18": {...

- Route: /station-report/A001/montlhy

{
  "01-2021": {
    "avg_gust": 4.77110215053763,
    "avg_speed": 1.9880376344086,
    "avg_max_humidity": 74.755376344086,
    "avg_min_humidity": 66.6854838709677,
    "avg_relative_humidity": 70.7567204301075,
    "avg_temperature_max": 23.045564516129,
    "avg_temperature_min": 21.5422043010753,
    "avg_temperature_max_dew": 16.8383064516129,
    "avg_temperature_min_dew": 15.4577956989247,
    "avg_accumulation": 0.351612903225806,
    "avg_radiation_global": 1751.90469135802,
    "avg_max_pressure": 886.261155913979,
    "avg_min_pressure": 885.745430107527,
    "avg_at_level_pressure": 886.000403225807
  },...

- Route: /station-report/A001/yearly

{
  "2021": {
    "avg_gust": 4.77110215053763,
    "avg_speed": 1.9880376344086,
    "avg_max_humidity": 74.755376344086,
    "avg_min_humidity": 66.6854838709677,
    "avg_relative_humidity": 70.7567204301075,
    "avg_temperature_max": 23.045564516129,
    "avg_temperature_min": 21.5422043010753,
    "avg_temperature_max_dew": 16.8383064516129,
    "avg_temperature_min_dew": 15.4577956989247,
    "avg_accumulation": 0.351612903225806,
    "avg_radiation_global": 1751.90469135803,
    "avg_max_pressure": 886.261155913979,
    "avg_min_pressure": 885.745430107527,
    "avg_at_level_pressure": 886.000403225807
  },
  "2020": {...

```

### GET /max_days_without_rain/<str:station>

- Route that calculates the maximum number of days without rain per season

```
- Route: max_days_without_rain/A001

{
  "BRASILIA": 75302
}

- Route: max_days_without_rain/

{
  "BRASILIA": 75302,
  "GOIANIA": 153833,
  "MORRINHOS": 132274,
  "SANTA MARIA": 147131,
  "SANTANA DO LIVRAMENTO": 122308,
  "FOZ DO IGUACU": 80877,
  "BOA VISTA": 69907,
  "BELEM": 123744,
  "CASTANHAL": 121861
}

```

### GET /max_min_humidity_months/<str:state>

- Route that finds the lowest and highest humidity months by state

```
- Route: max_min_humidity_months/PR

{
  "PR": [
    {
      "month": "09",
      "average_max_humidity": 89.0,
      "average_min_humidity": 19.0
    },
    {
      "month": "08",
      "average_max_humidity": 95.0,
      "average_min_humidity": 21.0
    },
    {
      "month": "07",
      "average_max_humidity": 93.0,
      "average_min_humidity": 27.0
    },
    {
      "month": "06",
      "average_max_humidity": 93.0,
      "average_min_humidity": 37.0
    },...


  - Route: max_min_humidity_months/
  [
  {
    "DF": [
      {
        "month": "01",
        "average_max_humidity": 96.0,
        "average_min_humidity": 28.0
      },
      {
        "month": "12",
        "average_max_humidity": 96.0,
        "average_min_humidity": 23.0
      },....
       {
    "GO": [
      {
        "month": "01",
        "average_max_humidity": 93.0,
        "average_min_humidity": 27.0
      },
      {
        "month": "12",
        "average_max_humidity": 94.0,
        "average_min_humidity": 19.0
      },....
```

### GET thermal_sensation/<str:station>/

- Route that calculates the thermal sensation for a certain time in a station.

```
- Route:/thermal_sensation/A001/?date=2021-01-05&time=17:00

{
  "station": "BRASILIA",
  "temperature": 26.6,
  "windspeed": 1.1,
  "sensation": 25.32298218410694
}

```

## `Workspace`

- Ubuntu 18.04.5 LTS
- Visual Studio Code 1.54.1
- Notbook Dell : Intel® Core™ i5-8265U CPU @ 1.60GHz × 8 , 7,7 GiB.
