from django.apps import AppConfig


class WithoutRainConfig(AppConfig):
    name = 'without_rain'
