from rainfall.models import Rainfall
from station.models import Station
from rest_framework.response import Response
from rest_framework.views import APIView
from django.shortcuts import get_object_or_404


class WithoutRainReportView(APIView):

    def get(self, request, station: str):

        is_valid = get_object_or_404(Station,
                                     code=station)

        all_rainfall = Rainfall.objects.filter(
            station__code=station).filter(accumulation_in_hour='0.0')

        response_data = {
            is_valid.name: all_rainfall.count()
        }

        return Response(response_data)


class WithoutRainReportAllView(APIView):

    def get(self, request):

        all_station = Station.objects.all()

        response_data = {}

        for station in all_station:

            all_rainfall = Rainfall.objects.filter(
                station=station).filter(accumulation_in_hour='0.0')

            response_data.update(
                {station.name: all_rainfall.count()}
            )

        return Response(response_data)
