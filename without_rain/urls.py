from django.urls import path
from .views import WithoutRainReportView, WithoutRainReportAllView


urlpatterns = [
    path('max_days_without_rain/<str:station>/',
         WithoutRainReportView.as_view()),
    path('max_days_without_rain/',
         WithoutRainReportAllView.as_view()),
]
