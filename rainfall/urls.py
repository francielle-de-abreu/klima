from .views import RainfallView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'rainfall', RainfallView)
urlpatterns = router.urls
