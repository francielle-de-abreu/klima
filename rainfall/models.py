from django.db import models
from station.models import Station
from moment.models import Moment


class Rainfall(models.Model):
    station = models.ForeignKey(
        Station, on_delete=models.CASCADE, related_name='rainfall')
    moment = models.ForeignKey(
        Moment, on_delete=models.CASCADE, related_name='rainfall')
    accumulation_in_hour = models.FloatField(null=True)

    def __str__(self):
        return f'{self.accumulation_in_hour}'
