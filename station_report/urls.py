from django.urls import path
from .views import StationReportView


urlpatterns = [
    path('station-report/<str:code>/<str:kind>', StationReportView.as_view()),

]
