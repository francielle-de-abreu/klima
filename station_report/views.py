from humidity.models import Humidity
from pressure.models import PressureMeasurement
from radiation.models import Radiation
from rainfall.models import Rainfall
from station.models import Station
from wind.models import WindMeasurement
from temperature.models import TemperatureMeasurement
from wind.models import WindMeasurement
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from django.shortcuts import get_object_or_404
from django.db.models import Avg
from django.db.models.functions import TruncWeek, TruncYear, TruncMonth
import itertools


class StationReportView(APIView):

    def get(self, request, code: str, kind: str):

        station = get_object_or_404(Station,
                                    code=code)

        all_humidity = Humidity.objects.filter(
            station=station).exclude(
            minimum_relative=None).exclude(
                minimum_relative='-9999.0').prefetch_related('moment', 'station')

        all_wind = WindMeasurement.objects.filter(
            station=station).exclude(
                direction=None).exclude(
                direction='-9999.0').prefetch_related('moment', 'station')

        all_temperature = TemperatureMeasurement.objects.filter(
            station=station).exclude(
                minimum=None).exclude(
                minimum='-9999.0').prefetch_related('moment', 'station')

        all_rainfall = Rainfall.objects.filter(
            station=station).exclude(
                accumulation_in_hour=None).exclude(
                accumulation_in_hour='-9999.0').prefetch_related('moment', 'station')

        all_radiation = Radiation.objects.filter(
            station=station).exclude(
                global_value=None).exclude(
                global_value='-9999.0').prefetch_related('moment', 'station')

        all_pressure = PressureMeasurement.objects.filter(
            station=station).exclude(
                minimum=None).exclude(
                minimum='-9999.0').prefetch_related('moment', 'station')

        # day

        wind_day = all_wind.values('moment__date').annotate(
            avg_gust=Avg('gust'), avg_speed=Avg('speed'))

        humidity_day = all_humidity.values('moment__date').annotate(
            avg_max_humidity=Avg('maximum_relative'), avg_min_humidity=Avg('minimum_relative'), avg_relative_humidity=Avg('relative'))

        temperature_day = all_temperature.values('moment__date').annotate(
            avg_temperature_max=Avg('maximum'), avg_temperature_min=Avg('minimum'), avg_temperature_max_dew=Avg('maximum_dew_point'), avg_temperature_min_dew=Avg('minimum_dew_point'))

        rainfall_day = all_rainfall.values('moment__date').annotate(
            avg_accumulation=Avg('accumulation_in_hour'))

        radiation_day = all_radiation.values('moment__date').annotate(
            avg_radiation_global=Avg('global_value'))

        pressure_day = all_pressure.values('moment__date').annotate(avg_max_pressure=Avg(
            'maximum'), avg_min_pressure=Avg('minimum'), avg_at_level_pressure=Avg('at_level'))

        # week

        wind_week = all_wind.annotate(
            week=TruncWeek('moment__date')).values('week').annotate(avg_gust=Avg('gust'), avg_speed=Avg('speed'))

        humidity_week = all_humidity.annotate(
            week=TruncWeek('moment__date')).values('week').annotate(avg_max_humidity=Avg('maximum_relative'), avg_min_humidity=Avg('minimum_relative'), avg_relative_humidity=Avg('relative'))

        temperature_week = all_temperature.annotate(
            week=TruncWeek('moment__date')).values('week').annotate(avg_temperature_max=Avg('maximum'), avg_temperature_min=Avg('minimum'), avg_temperature_max_dew=Avg('maximum_dew_point'), avg_temperature_min_dew=Avg('minimum_dew_point'))

        rainfall_week = all_rainfall.annotate(
            week=TruncWeek('moment__date')).values('week').annotate(avg_accumulation=Avg('accumulation_in_hour'))

        radiation_week = all_radiation.annotate(
            week=TruncWeek('moment__date')).values('week').annotate(avg_radiation_global=Avg('global_value'))

        pressure_week = all_pressure.annotate(
            week=TruncWeek('moment__date')).values('week').annotate(avg_max_pressure=Avg('maximum'), avg_min_pressure=Avg('minimum'), avg_at_level_pressure=Avg('at_level'))

        # month

        wind_month = wind_day.annotate(month=TruncMonth('moment__date')).values(
            'month').annotate(avg_gust=Avg('gust'), avg_speed=Avg('speed'))

        humidity_month = humidity_day.annotate(month=TruncMonth('moment__date')).values(
            'month').annotate(avg_max_humidity=Avg(
                'maximum_relative'), avg_min_humidity=Avg('minimum_relative'), avg_relative_humidity=Avg('relative'))

        temperature_month = temperature_day.annotate(month=TruncMonth('moment__date')).values(
            'month').annotate(avg_temperature_max=Avg('maximum'), avg_temperature_min=Avg('minimum'), avg_temperature_max_dew=Avg('maximum_dew_point'), avg_temperature_min_dew=Avg('minimum_dew_point'))

        rainfall_month = rainfall_day.annotate(month=TruncMonth('moment__date')).values(
            'month').annotate(avg_accumulation=Avg('accumulation_in_hour'))

        radiation_month = radiation_day.annotate(month=TruncMonth('moment__date')).values(
            'month').annotate(avg_radiation_global=Avg('global_value'))

        pressure_month = pressure_day.annotate(month=TruncMonth('moment__date')).values(
            'month').annotate(
            avg_max_pressure=Avg('maximum'), avg_min_pressure=Avg('minimum'), avg_at_level_pressure=Avg('at_level'))

        # year

        wind_year = wind_day.annotate(year=TruncYear('moment__date')).values(
            'year').annotate(avg_gust=Avg('gust'), avg_speed=Avg('speed'))

        humidity_year = humidity_day.annotate(year=TruncYear('moment__date')).values(
            'year').annotate(avg_max_humidity=Avg(
                'maximum_relative'), avg_min_humidity=Avg('minimum_relative'), avg_relative_humidity=Avg('relative'))

        temperature_year = temperature_day.annotate(year=TruncYear('moment__date')).values(
            'year').annotate(avg_temperature_max=Avg('maximum'), avg_temperature_min=Avg('minimum'), avg_temperature_max_dew=Avg('maximum_dew_point'), avg_temperature_min_dew=Avg('minimum_dew_point'))

        rainfall_year = rainfall_day.annotate(year=TruncYear('moment__date')).values(
            'year').annotate(avg_accumulation=Avg('accumulation_in_hour'))

        radiation_year = radiation_day.annotate(year=TruncYear('moment__date')).values(
            'year').annotate(avg_radiation_global=Avg('global_value'))

        pressure_year = pressure_day.annotate(year=TruncYear('moment__date')).values(
            'year').annotate(
            avg_max_pressure=Avg('maximum'), avg_min_pressure=Avg('minimum'), avg_at_level_pressure=Avg('at_level'))

        if kind == 'daily':

            response_data = {}

            for (w, h, t, r, rd, p) in itertools.zip_longest(wind_day.order_by('-moment__date'), humidity_day.order_by(
                '-moment__date'),
                temperature_day.order_by(
                    '-moment__date'),
                    rainfall_day.order_by(
                    '-moment__date'),
                    radiation_day.order_by('-moment__date'),
                    pressure_day.order_by('-moment__date')):

                if h is not None and w is not None:

                    if w['moment__date'] not in response_data and w['moment__date'] != h['moment__date']:
                        response_data[str(h['moment__date'])] = {}

                    if w['moment__date'] not in response_data:
                        response_data[str(w['moment__date'])] = {}

                    response_data[str(w['moment__date'])
                                  ]['avg_gust'] = w['avg_gust']
                    response_data[str(w['moment__date'])
                                  ]['avg_speed'] = w['avg_speed']

                if w is not None and h is not None:

                    if h['moment__date'] not in response_data and w['moment__date'] != h['moment__date']:
                        response_data[str(h['moment__date'])] = {}

                    response_data[str(h['moment__date'])
                                  ]['avg_max_humidity'] = h['avg_max_humidity']
                    response_data[str(h['moment__date'])
                                  ]['avg_min_humidity'] = h['avg_min_humidity']
                    response_data[str(
                        h['moment__date'])]['avg_relative_humidity'] = h['avg_relative_humidity']

                if w is not None and h is not None and t is not None:

                    if t['moment__date'] not in response_data and w['moment__date'] != t['moment__date'] \
                            and h['moment__date'] != t['moment__date']:
                        response_data[str(t['moment__date'])] = {}

                    response_data[str(t['moment__date'])
                                  ]['avg_temperature_max'] = t['avg_temperature_max']
                    response_data[str(t['moment__date'])
                                  ]['avg_temperature_min'] = t['avg_temperature_min']
                    response_data[str(
                        t['moment__date'])]['avg_temperature_max_dew'] = t['avg_temperature_max_dew']
                    response_data[str(
                        t['moment__date'])]['avg_temperature_min_dew'] = t['avg_temperature_min_dew']

                if w is not None and h is not None and t is not None and r is not None:

                    if r['moment__date'] not in response_data and w['moment__date'] != r['moment__date'] \
                            and h['moment__date'] != r['moment__date'] and t['moment__date'] != r['moment__date']:
                        response_data[str(r['moment__date'])] = {}

                    response_data[str(r['moment__date'])
                                  ]['avg_accumulation'] = r['avg_accumulation']

                if w is not None and h is not None and t is not None and r is not None and rd is not None:

                    if rd['moment__date'] not in response_data and w['moment__date'] != rd['moment__date'] \
                            and h['moment__date'] != rd['moment__date'] and t['moment__date'] != rd['moment__date'] \
                            and r['moment__date'] != rd['moment__date']:

                        response_data[str(rd['moment__date'])] = {}

                    response_data[str(rd['moment__date'])
                                  ]['avg_radiation_global'] = rd['avg_radiation_global']

                if w is not None and h is not None and t is not None and r is not None and rd is not None and p is not None:

                    if p['moment__date'] not in response_data and w['moment__date'] != p['moment__date'] \
                            and h['moment__date'] != p['moment__date'] and t['moment__date'] != p['moment__date'] \
                            and r['moment__date'] != p['moment__date'] and rd['moment__date'] != p['moment__date']:

                        response_data[str(p['moment__date'])] = {}

                    response_data[str(p['moment__date'])
                                  ]['avg_max_pressure'] = p['avg_max_pressure']
                    response_data[str(p['moment__date'])
                                  ]['avg_min_pressure'] = p['avg_min_pressure']
                    response_data[str(p['moment__date'])
                                  ]['avg_at_level_pressure'] = p['avg_at_level_pressure']

            return Response(response_data)

        if kind == 'weekly':

            response_data = {}

            for (w, h, t, r, rd, p) in itertools.zip_longest(wind_week.order_by('-week'), humidity_week.order_by(
                '-week'),
                temperature_week.order_by(
                    '-week'),
                    rainfall_week.order_by(
                    '-week'),
                    radiation_week.order_by('-week'),
                    pressure_week.order_by('-week')):

                if h is not None and w is not None:

                    if w['week'] not in response_data and w['week'] != h['week']:
                        response_data[str(h['week'])] = {}

                    if w['week'] not in response_data:
                        response_data[str(w['week'])] = {}

                    response_data[str(w['week'])
                                  ]['avg_gust'] = w['avg_gust']
                    response_data[str(w['week'])
                                  ]['avg_speed'] = w['avg_speed']

                if w is not None and h is not None:

                    if h['week'] not in response_data and w['week'] != h['week']:
                        response_data[str(h['week'])] = {}

                    response_data[str(h['week'])
                                  ]['avg_max_humidity'] = h['avg_max_humidity']
                    response_data[str(h['week'])
                                  ]['avg_min_humidity'] = h['avg_min_humidity']
                    response_data[str(
                        h['week'])]['avg_relative_humidity'] = h['avg_relative_humidity']

                if w is not None and h is not None and t is not None:

                    if t['week'] not in response_data and w['week'] != t['week'] \
                            and h['week'] != t['week']:
                        response_data[str(t['week'])] = {}

                    response_data[str(t['week'])
                                  ]['avg_temperature_max'] = t['avg_temperature_max']
                    response_data[str(t['week'])
                                  ]['avg_temperature_min'] = t['avg_temperature_min']
                    response_data[str(
                        t['week'])]['avg_temperature_max_dew'] = t['avg_temperature_max_dew']
                    response_data[str(
                        t['week'])]['avg_temperature_min_dew'] = t['avg_temperature_min_dew']

                if w is not None and h is not None and t is not None and r is not None:

                    if r['week'] not in response_data and w['week'] != r['week'] \
                            and h['week'] != r['week'] and t['week'] != r['week']:
                        response_data[str(r['week'])] = {}

                    response_data[str(r['week'])
                                  ]['avg_accumulation'] = r['avg_accumulation']

                if w is not None and h is not None and t is not None and r is not None and rd is not None:

                    if rd['week'] not in response_data and w['week'] != rd['week'] \
                            and h['week'] != rd['week'] and t['week'] != rd['week'] \
                            and r['week'] != rd['week']:

                        response_data[str(rd['week'])] = {}

                    response_data[str(rd['week'])
                                  ]['avg_radiation_global'] = rd['avg_radiation_global']

                if w is not None and h is not None and t is not None and r is not None and rd is not None and p is not None:

                    if p['week'] not in response_data and w['week'] != p['week'] \
                            and h['week'] != p['week'] and t['week'] != p['week'] \
                            and r['week'] != p['week'] and rd['week'] != p['week']:

                        response_data[str(p['week'])] = {}

                    response_data[str(p['week'])
                                  ]['avg_max_pressure'] = p['avg_max_pressure']
                    response_data[str(p['week'])
                                  ]['avg_min_pressure'] = p['avg_min_pressure']
                    response_data[str(p['week'])
                                  ]['avg_at_level_pressure'] = p['avg_at_level_pressure']

            return Response(response_data)

        if kind == 'montlhy':

            response_data = {}

            for (w, h, t, r, rd, p) in itertools.zip_longest(wind_month.order_by('-month'), humidity_month.order_by(
                '-month'),
                temperature_month.order_by(
                    '-month'),
                    rainfall_month.order_by(
                    '-month'),
                    radiation_month.order_by('-month'),
                    pressure_month.order_by('-month')):

                if h is not None and w is not None:

                    if w['month'].strftime("%m-%Y") not in response_data and w['month'].strftime("%m-%Y") != h['month'].strftime("%m-%Y"):
                        response_data[str(h['month'].strftime("%m-%Y"))] = {}

                    if w['month'].strftime("%m-%Y") not in response_data:
                        response_data[str(w['month'].strftime("%m-%Y"))] = {}

                    response_data[str(w['month'].strftime("%m-%Y"))
                                  ]['avg_gust'] = w['avg_gust']
                    response_data[str(w['month'].strftime("%m-%Y"))
                                  ]['avg_speed'] = w['avg_speed']

                if w is not None and h is not None:

                    if h['month'].strftime("%m-%Y") not in response_data and w['month'].strftime("%m-%Y") != h['month'].strftime("%m-%Y"):
                        response_data[str(h['month'].strftime("%m-%Y"))] = {}

                    response_data[str(h['month'].strftime("%m-%Y"))
                                  ]['avg_max_humidity'] = h['avg_max_humidity']
                    response_data[str(h['month'].strftime("%m-%Y"))
                                  ]['avg_min_humidity'] = h['avg_min_humidity']
                    response_data[str(
                        h['month'].strftime("%m-%Y"))]['avg_relative_humidity'] = h['avg_relative_humidity']

                if w is not None and h is not None and t is not None:

                    if t['month'].strftime("%m-%Y") not in response_data and w['month'].strftime("%m-%Y") != t['month'].strftime("%m-%Y") \
                            and h['month'].strftime("%m-%Y") != t['month'].strftime("%m-%Y"):
                        response_data[str(t['month'].strftime("%m-%Y"))] = {}

                    response_data[str(t['month'].strftime("%m-%Y"))
                                  ]['avg_temperature_max'] = t['avg_temperature_max']
                    response_data[str(t['month'].strftime("%m-%Y"))
                                  ]['avg_temperature_min'] = t['avg_temperature_min']
                    response_data[str(
                        t['month'].strftime("%m-%Y"))]['avg_temperature_max_dew'] = t['avg_temperature_max_dew']
                    response_data[str(
                        t['month'].strftime("%m-%Y"))]['avg_temperature_min_dew'] = t['avg_temperature_min_dew']

                if w is not None and h is not None and t is not None and r is not None:

                    if r['month'].strftime("%m-%Y") not in response_data and w['month'].strftime("%m-%Y") != r['month'].strftime("%m-%Y") \
                            and h['month'].strftime("%m-%Y") != r['month'].strftime("%m-%Y") and t['month'].strftime("%m-%Y") != r['month'].strftime("%m-%Y"):
                        response_data[str(r['month'].strftime("%m-%Y"))] = {}

                    response_data[str(r['month'].strftime("%m-%Y"))
                                  ]['avg_accumulation'] = r['avg_accumulation']

                if w is not None and h is not None and t is not None and r is not None and rd is not None:

                    if rd['month'].strftime("%m-%Y") not in response_data and w['month'].strftime("%m-%Y") != rd['month'].strftime("%m-%Y") \
                            and h['month'].strftime("%m-%Y") != rd['month'].strftime("%m-%Y") and t['month'].strftime("%m-%Y") != rd['month'].strftime("%m-%Y") \
                            and r['month'].strftime("%m-%Y") != rd['month'].strftime("%m-%Y"):

                        response_data[str(rd['month'].strftime("%m-%Y"))] = {}

                    response_data[str(rd['month'].strftime("%m-%Y"))
                                  ]['avg_radiation_global'] = rd['avg_radiation_global']

                if w is not None and h is not None and t is not None and r is not None and rd is not None and p is not None:

                    if p['month'].strftime("%m-%Y") not in response_data and w['month'].strftime("%m-%Y") != p['month'].strftime("%m-%Y") \
                            and h['month'].strftime("%m-%Y") != p['month'].strftime("%m-%Y") and t['month'].strftime("%m-%Y") != p['month'].strftime("%m-%Y") \
                            and r['month'].strftime("%m-%Y") != p['month'].strftime("%m-%Y") and rd['month'].strftime("%m-%Y") != p['month'].strftime("%m-%Y"):

                        response_data[str(p['month'].strftime("%m-%Y"))] = {}

                    response_data[str(p['month'].strftime("%m-%Y"))
                                  ]['avg_max_pressure'] = p['avg_max_pressure']
                    response_data[str(p['month'].strftime("%m-%Y"))
                                  ]['avg_min_pressure'] = p['avg_min_pressure']
                    response_data[str(p['month'].strftime("%m-%Y"))
                                  ]['avg_at_level_pressure'] = p['avg_at_level_pressure']

            return Response(response_data)

        if kind == 'yearly':

            response_data = {}

            for (w, h, t, r, rd, p) in itertools.zip_longest(wind_year.order_by('-year'), humidity_year.order_by(
                '-year'),
                temperature_year.order_by(
                    '-year'),
                    rainfall_year.order_by(
                    '-year'),
                    radiation_year.order_by('-year'),
                    pressure_year.order_by('-year')):

                if h is not None and w is not None:

                    if w['year'].strftime("%Y") not in response_data and w['year'].strftime("%Y") != h['year'].strftime("%Y"):
                        response_data[str(h['year'].strftime("%Y"))] = {}

                    if w['year'].strftime("%Y") not in response_data:
                        response_data[str(w['year'].strftime("%Y"))] = {}

                    response_data[str(w['year'].strftime("%Y"))
                                  ]['avg_gust'] = w['avg_gust']
                    response_data[str(w['year'].strftime("%Y"))
                                  ]['avg_speed'] = w['avg_speed']

                if w is not None and h is not None:

                    if h['year'].strftime("%Y") not in response_data and w['year'].strftime("%Y") != h['year'].strftime("%Y"):
                        response_data[str(h['year'].strftime("%Y"))] = {}

                    response_data[str(h['year'].strftime("%Y"))
                                  ]['avg_max_humidity'] = h['avg_max_humidity']
                    response_data[str(h['year'].strftime("%Y"))
                                  ]['avg_min_humidity'] = h['avg_min_humidity']
                    response_data[str(
                        h['year'].strftime("%Y"))]['avg_relative_humidity'] = h['avg_relative_humidity']

                if w is not None and h is not None and t is not None:

                    if t['year'].strftime("%Y") not in response_data and w['year'].strftime("%Y") != t['year'].strftime("%Y") \
                            and h['year'].strftime("%Y") != t['year'].strftime("%Y"):
                        response_data[str(t['year'].strftime("%Y"))] = {}

                    response_data[str(t['year'].strftime("%Y"))
                                  ]['avg_temperature_max'] = t['avg_temperature_max']
                    response_data[str(t['year'].strftime("%Y"))
                                  ]['avg_temperature_min'] = t['avg_temperature_min']
                    response_data[str(
                        t['year'].strftime("%Y"))]['avg_temperature_max_dew'] = t['avg_temperature_max_dew']
                    response_data[str(
                        t['year'].strftime("%Y"))]['avg_temperature_min_dew'] = t['avg_temperature_min_dew']

                if w is not None and h is not None and t is not None and r is not None:

                    if r['year'].strftime("%Y") not in response_data and w['year'].strftime("%Y") != r['year'].strftime("%Y") \
                            and h['year'].strftime("%Y") != r['year'].strftime("%Y") and t['year'].strftime("%Y") != r['year'].strftime("%Y"):
                        response_data[str(r['year'].strftime("%Y"))] = {}

                    response_data[str(r['year'].strftime("%Y"))
                                  ]['avg_accumulation'] = r['avg_accumulation']

                if w is not None and h is not None and t is not None and r is not None and rd is not None:

                    if rd['year'].strftime("%Y") not in response_data and w['year'].strftime("%Y") != rd['year'].strftime("%Y") \
                            and h['year'].strftime("%Y") != rd['year'].strftime("%Y") and t['year'].strftime("%Y") != rd['year'].strftime("%Y") \
                            and r['year'].strftime("%Y") != rd['year'].strftime("%Y"):

                        response_data[str(rd['year'].strftime("%Y"))] = {}

                    response_data[str(rd['year'].strftime("%Y"))
                                  ]['avg_radiation_global'] = rd['avg_radiation_global']

                if w is not None and h is not None and t is not None and r is not None and rd is not None and p is not None:

                    if p['year'].strftime("%Y") not in response_data and w['year'].strftime("%Y") != p['year'].strftime("%Y") \
                            and h['year'].strftime("%Y") != p['year'].strftime("%Y") and t['year'].strftime("%Y") != p['year'].strftime("%Y") \
                            and r['year'].strftime("%Y") != p['year'].strftime("%Y") and rd['year'].strftime("%Y") != p['year'].strftime("%Y"):

                        response_data[str(p['year'].strftime("%Y"))] = {}

                    response_data[str(p['year'].strftime("%Y"))
                                  ]['avg_max_pressure'] = p['avg_max_pressure']
                    response_data[str(p['year'].strftime("%Y"))
                                  ]['avg_min_pressure'] = p['avg_min_pressure']
                    response_data[str(p['year'].strftime("%Y"))
                                  ]['avg_at_level_pressure'] = p['avg_at_level_pressure']

            return Response(response_data)

        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
