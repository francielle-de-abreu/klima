from django.apps import AppConfig


class StationReportConfig(AppConfig):
    name = 'station_report'
