from django.shortcuts import render
from rest_framework.mixins import ListModelMixin, CreateModelMixin, RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin
from rest_framework.viewsets import GenericViewSet
from .models import State
from .serializers import StateSerializer


class StateView(GenericViewSet,
                RetrieveModelMixin,
                CreateModelMixin,
                UpdateModelMixin,
                DestroyModelMixin,
                ListModelMixin):

    queryset = State.objects.all()
    serializer_class = StateSerializer
