from .views import StateView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'state', StateView)
urlpatterns = router.urls
