from django.db import models


class Moment(models.Model):
    date = models.DateField()
    time = models.TimeField()

    def __str__(self):
        return f'{self.date} - {self.time}'
