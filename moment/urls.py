from .views import MomentView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'moment', MomentView)
urlpatterns = router.urls
