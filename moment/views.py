from rest_framework.mixins import ListModelMixin, CreateModelMixin, RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin
from rest_framework.viewsets import GenericViewSet
from .models import Moment
from .serializers import MomentSerializer
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page


class MomentView(GenericViewSet,
                 RetrieveModelMixin,
                 CreateModelMixin,
                 UpdateModelMixin,
                 DestroyModelMixin,
                 ListModelMixin):

    @method_decorator(cache_page(10))
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    queryset = Moment.objects.all()
    serializer_class = MomentSerializer
