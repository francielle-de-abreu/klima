
from rainfall.models import Rainfall
from station.models import Station
from region.models import Region
from state.models import State
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from datetime import datetime


class PrecipitationView(APIView):

    def get(self, request, agrupamento: str):

        if agrupamento == 'station':
            types = Station.objects.all()
        elif agrupamento == 'state':
            types = State.objects.all()
        elif agrupamento == 'region':
            types = Region.objects.all()
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        year = request.GET.get("year", None)

        month = request.GET.get("month", None)

        if month is not None and int(month) < 10:
            month = f'0{month}'

        day = request.GET.get("day", None)

        if day is not None and int(day) < 10:
            day = f'0{day}'

        listresponse = []

        if year is not None and month is None and day is None:

            for type in types:

                rainfalls = Rainfall.objects.filter(
                    moment__date__icontains=year).filter(
                    station=type.id).exclude(accumulation_in_hour=None).exclude(accumulation_in_hour='-9999.0').prefetch_related('station', 'moment')

                lista = []

                for rainfall in rainfalls:
                    lista.append(float(rainfall.accumulation_in_hour))

                details = {
                    type.name: {
                        'accumulation_in_hour__sum': sum(lista)
                    }
                }

                listresponse.append(details)

        if year is not None and month is not None and day is None:

            for type in types:

                rainfalls = Rainfall.objects.filter(
                    moment__date__icontains=f'{year}-{month}').filter(
                    station=type.id).exclude(accumulation_in_hour=None).exclude(accumulation_in_hour='-9999.0').prefetch_related('station', 'moment')

                lista = []

                for rainfall in rainfalls:
                    lista.append(float(rainfall.accumulation_in_hour))

                details = {
                    type.name: {
                        'accumulation_in_hour__sum': sum(lista)
                    }
                }

                listresponse.append(details)

        if year is not None and month is not None and day is not None:

            for type in types:

                rainfalls = Rainfall.objects.filter(
                    moment__date=f'{year}-{month}-{day}').filter(
                    station=type.id).exclude(accumulation_in_hour=None).exclude(accumulation_in_hour='-9999.0').prefetch_related('station', 'moment')

                lista = []

                for rainfall in rainfalls:

                    lista.append(float(rainfall.accumulation_in_hour))

                details = {
                    type.name: {
                        'accumulation_in_hour__sum': sum(lista)
                    }
                }

                listresponse.append(details)

        if day is None and month is None and year is None:

            for type in types:

                data = datetime.now()

                rainfalls = Rainfall.objects.filter(
                    moment__date=data.strftime("%Y"'-'"%m"'-'"%d")).filter(
                    station=type.id).exclude(accumulation_in_hour=None).exclude(accumulation_in_hour='-9999.0').prefetch_related('station', 'moment')

                lista = []

                for rainfall in rainfalls:

                    lista.append(float(rainfall.accumulation_in_hour))

                details = {
                    type.name: {
                        'accumulation_in_hour__sum': sum(lista)
                    }
                }

                listresponse.append(details)

        return Response(listresponse)
