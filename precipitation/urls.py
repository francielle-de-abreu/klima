from django.urls import path
from .views import PrecipitationView


urlpatterns = [
    path('precipitation/<str:agrupamento>/', PrecipitationView.as_view()),

]
