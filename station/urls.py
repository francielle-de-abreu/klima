from .views import StationView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'station', StationView)
urlpatterns = router.urls
