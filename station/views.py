from django.shortcuts import render
from rest_framework.mixins import ListModelMixin, CreateModelMixin, RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin
from rest_framework.viewsets import GenericViewSet
from .models import Station
from .serializers import StationSerializer


class StationView(GenericViewSet,
                  RetrieveModelMixin,
                  UpdateModelMixin,
                  DestroyModelMixin,
                  CreateModelMixin,
                  ListModelMixin):

    queryset = Station.objects.all()
    serializer_class = StationSerializer
