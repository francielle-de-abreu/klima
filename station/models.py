from django.db import models
from state.models import State
from region.models import Region


class Station(models.Model):
    name = models.CharField(max_length=255)
    region = models.ForeignKey(
        Region, on_delete=models.CASCADE, related_name='station')
    state = models.ForeignKey(
        State, on_delete=models.CASCADE, related_name='station')
    latitude = models.FloatField(null=True)
    longitude = models.FloatField(null=True)
    altitude = models.FloatField(null=True)
    code = models.CharField(max_length=255)

    def __str__(self):
        return f'{self.name}'
