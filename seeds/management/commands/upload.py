from django.core.management.base import BaseCommand
from region.models import Region
from station.models import Station
from state.models import State
from moment.models import Moment
from humidity.models import Humidity
from temperature.models import TemperatureMeasurement
from pressure.models import PressureMeasurement
from wind.models import WindMeasurement
from rainfall.models import Rainfall
from radiation.models import Radiation
from humidity.models import Humidity
import csv
from datetime import date, time
from klima.settings import BASE_DIR
import os
from multiprocessing import Process
import time as slp
from django import db


class Command(BaseCommand):
    help = 'Populates the db with weather data'

    def insert_stations(self):
        csvfile = "stations.csv"

        with open(csvfile) as file:
            stations_all = csv.DictReader(file, delimiter=";")

            station_obj_list = []

            for item in stations_all:
                region = Region.objects.get_or_create(name=item["REGIAO"])[0]
                state = State.objects.get_or_create(name=item["UF"])[0]

                print(f'Inserindo estacao {item["ESTACAO"]}')
                station_obj = Station(name=item["ESTACAO"],
                                      code=item["CODIGO"],
                                      region=region,
                                      state=state,
                                      latitude=item["LATITUDE"],
                                      longitude=item["LONGITUDE"],
                                      altitude=item["ALTITUDE"])
                station_obj_list.append(station_obj)

            Station.objects.bulk_create(station_obj_list)

    def insert_weather(self, csvfile):
        db.close_old_connections()

        print(f" --- Organizando dados do arquivo {csvfile} para inserção ---")

        stations_queryset = Station.objects.all()

        fieldnames = [
            "station", "day", "hour", "precipitation", "pressure",
            "max_pressure", "min_pressure", "radiation", "air_temperature",
            "dew_point", "max_temperature", "min_temperature", "max_dew_point",
            "min_dew_point", "max_humidity", "min_humidity", "humidity",
            "wind_direction", "wind_gust", "wind_speed"
        ]

        humidity_obj_list = []
        temperature_obj_list = []
        pressure_obj_list = []
        wind_obj_list = []
        rain_obj_list = []
        radiation_obj_list = []

        with open(csvfile) as file:
            weather_all = csv.DictReader(file, delimiter=",",
                                         fieldnames=fieldnames)

            next(weather_all, None)  # Ignorando a primeira linha do arquivo

            i = 0
            # Impressão para acompanhar a execução do script a cada 1000 linhas
            for item in weather_all:
                i += 1
                if i % 1000 == 0:
                    print(i)

                # Verificando se existem dados de medição a serem inseridos.
                # Se todos os valores de medição forem vazios, não insere a
                #  linha corrente
                insert = False
                for key in item.keys():
                    if item[key] != '':
                        insert = True
                        break

                if not insert:
                    continue

                day = date.fromisoformat(item["day"])
                hour = time(int(int(item["hour"])/100))

                try:
                    moment = Moment.objects.get_or_create(
                        date=day, time=hour)[0]
                except Exception:
                    moment = Moment.objects.filter(date=day, time=hour).first()

                station = stations_queryset.get(code=item["station"])

                humidity_obj = \
                    Humidity(
                        station=station,
                        moment=moment,
                        maximum_relative=item["max_humidity"]
                        if item["max_humidity"] != '' else None,
                        minimum_relative=item["min_humidity"]
                        if item["min_humidity"] != '' else None,
                        relative=item["humidity"]
                        if item["humidity"] != '' else None
                    )
                humidity_obj_list.append(humidity_obj)

                temperature_obj = \
                    TemperatureMeasurement(
                        station=station,
                        moment=moment,
                        air=item["air_temperature"]
                        if item["air_temperature"] != '' else None,
                        dew_point=item["dew_point"]
                        if item["dew_point"] != '' else None,
                        maximum=item["max_temperature"]
                        if item["max_temperature"] != '' else None,
                        minimum=item["min_temperature"]
                        if item["min_temperature"] != '' else None,
                        maximum_dew_point=item["max_dew_point"]
                        if item["max_dew_point"] != '' else None,
                        minimum_dew_point=item["min_dew_point"]
                        if item["min_dew_point"] != '' else None
                    )
                temperature_obj_list.append(temperature_obj)

                pressure_obj = \
                    PressureMeasurement(
                        station=station,
                        moment=moment,
                        at_level=item["pressure"]
                        if item["pressure"] != '' else None,
                        maximum=item["max_pressure"]
                        if item["max_pressure"] != '' else None,
                        minimum=item["min_pressure"]
                        if item["min_pressure"] != '' else None
                    )
                pressure_obj_list.append(pressure_obj)

                wind_obj = \
                    WindMeasurement(
                        station=station,
                        moment=moment,
                        direction=item["wind_direction"]
                        if item["wind_direction"] != '' else None,
                        gust=item["wind_gust"]
                        if item["wind_gust"] != '' else None,
                        speed=item["wind_speed"]
                        if item["wind_speed"] != '' else None
                    )
                wind_obj_list.append(wind_obj)

                rain_obj = \
                    Rainfall(
                        station=station,
                        moment=moment,
                        accumulation_in_hour=item["precipitation"]
                        if item["precipitation"] != '' else None
                    )
                rain_obj_list.append(rain_obj)

                radiation_obj = \
                    Radiation(
                        station=station,
                        moment=moment,
                        global_value=item["radiation"]
                        if item["radiation"] != '' else None
                    )
                radiation_obj_list.append(radiation_obj)

            print(f" --- Dados organizados. Inserindo {csvfile} --- ")
            Humidity.objects.bulk_create(humidity_obj_list)
            TemperatureMeasurement.objects.bulk_create(temperature_obj_list)
            PressureMeasurement.objects.bulk_create(pressure_obj_list)
            WindMeasurement.objects.bulk_create(wind_obj_list)
            Rainfall.objects.bulk_create(rain_obj_list)
            Radiation.objects.bulk_create(radiation_obj_list)

    def handle(self, *args, **kwargs):

        self.insert_stations()
        slp.sleep(15)

        files = os.listdir(os.path.join(BASE_DIR, "weather_data"))

        for file in files:
            csvfile = os.path.join(BASE_DIR, "weather_data/")+file
            p = Process(target=self.insert_weather, args=(csvfile,))
            p.start()
            slp.sleep(15)
