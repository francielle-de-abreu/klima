from django.apps import AppConfig


class MinAndMaxConfig(AppConfig):
    name = 'min_and_max'
