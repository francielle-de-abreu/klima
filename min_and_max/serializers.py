from rest_framework import serializers
from .models import MinMax


class MinMaxSerializer(serializers.ModelSerializer):
    class Meta:
        model = MinMax
        fields = '__all__'
