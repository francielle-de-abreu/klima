from django.shortcuts import get_object_or_404
from django.db.models.functions import TruncMonth
from rest_framework.views import APIView
from django.db.models import Max, Min
from humidity.models import Humidity
from pressure.models import PressureMeasurement
from radiation.models import Radiation
from rainfall.models import Rainfall
from station.models import Station
from wind.models import WindMeasurement
from temperature.models import TemperatureMeasurement
from region.models import Region
from state.models import State
from rest_framework.generics import ListAPIView
from wind.models import WindMeasurement
from rest_framework.response import Response
from rest_framework import status
from .models import MinMax
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page


class MinMaxView(ListAPIView):

    def list(self, request,  agrupamento=None, *args, **kwargs):

        if agrupamento == 'station':
            types = Station.objects.all()
        elif agrupamento == 'state':
            types = State.objects.all()
        elif agrupamento == 'region':
            types = Region.objects.all()
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        listresponse = []

        for type in types:

            humidity = Humidity.objects.filter(
                station=type.id).prefetch_related('station', 'moment')
            list_max_humidity = humidity.exclude(maximum_relative=None).order_by(
                '-maximum_relative').first()
            moment_max_humidity = list_max_humidity.moment
            list_min_humidity = humidity.exclude(
                minimum_relative=None).order_by('-minimum_relative').first()
            moment_min_humidity = list_min_humidity.moment

            pressure = PressureMeasurement.objects.filter(
                station=type.id).prefetch_related('station', 'moment')
            list_max_pressure = pressure.exclude(
                maximum=None).order_by('-maximum').first()
            moment_max_pressure = list_max_pressure.moment
            list_min_pressure = pressure.exclude(
                minimum=None).exclude(
                minimum='-9999.0').order_by('minimum').first()
            moment_min_pressure = list_min_pressure.moment

            radiation = Radiation.objects.filter(
                station=type.id).prefetch_related('station', 'moment')
            list_max_radiation = radiation.exclude(
                global_value=None).order_by('-global_value').first()
            moment_max_radiation = list_max_radiation.moment
            list_min_radiation = radiation.exclude(
                global_value=None).exclude(
                global_value='-9999.0').order_by('global_value').first()
            moment_min_radiation = list_min_radiation.moment

            rainfall = Rainfall.objects.filter(
                station=type.id).prefetch_related('station', 'moment')
            list_max_rainfall = rainfall.exclude(accumulation_in_hour=None).order_by(
                '-accumulation_in_hour').first()
            moment_max_rainfall = list_max_rainfall.moment
            list_min_rainfall = rainfall.exclude(accumulation_in_hour=None).exclude(
                accumulation_in_hour='-9999.0').order_by(
                'accumulation_in_hour').first()
            moment_min_rainfall = list_min_rainfall.moment

            temperature = TemperatureMeasurement.objects.filter(
                station=type.id).prefetch_related('station', 'moment')
            list_max_temperature = temperature.exclude(
                maximum=None).order_by('-maximum').first()
            moment_max_temperature = list_max_temperature.moment
            list_min_temperature = temperature.exclude(
                minimum=None).exclude(
                minimum='-9999.0').order_by('minimum').first()
            moment_min_temperature = list_min_temperature.moment

            wind = WindMeasurement.objects.filter(
                station=type.id).prefetch_related('station', 'moment')
            list_max_wind = wind.exclude(
                direction=None).order_by('-direction').first()
            moment_max_wind = list_max_wind.moment
            list_min_wind = wind.exclude(
                direction=None).exclude(
                direction='-9999.0').order_by('direction').first()
            moment_min_wind = list_min_wind.moment

            details = {
                type.name: {
                    'pressure': {
                        "max": {
                            "value": list_max_pressure.maximum,
                            "date": f'{moment_max_pressure.date} {moment_max_pressure.time}'
                        },
                        "min": {
                            "value": list_min_pressure.minimum,
                            "date": f'{moment_min_pressure.date} {moment_min_pressure.time}'
                        },
                    },
                    'radiation':  {
                        "max": {
                            "value": list_max_radiation.global_value,
                            "date": f'{moment_max_radiation.date} {moment_max_radiation.time}'
                        },
                        "min": {
                            "value": list_min_radiation.global_value,
                            "date": f'{moment_min_radiation.date} {moment_max_radiation.time}'
                        },
                    },
                    'rainfall': {
                        "max": {
                            "value": list_max_rainfall.accumulation_in_hour,
                            "date": f'{moment_max_rainfall.date} {moment_max_rainfall.time}'
                        },
                        "min": {
                            "value": list_min_rainfall.accumulation_in_hour,
                            "date": f'{moment_min_rainfall.date} {moment_min_rainfall.time}'
                        },
                    },
                    'temperature': {
                        "max": {
                            "value": list_max_temperature.maximum,
                            "date":  f'{moment_max_temperature.date} {moment_max_temperature.time}'
                        },
                        "min": {
                            "value": list_min_temperature.minimum,
                            "date": f'{moment_min_temperature.date} {moment_min_temperature.time}'
                        },
                    },
                    'wind': {
                        "max": {
                            "value": list_max_wind.direction,
                            "date": f'{moment_max_wind.date} {moment_max_wind.time}'
                        },
                        "min": {
                            "value": list_min_wind.direction,
                            "date":  f'{moment_min_wind.date} {moment_min_wind.time}'
                        },
                    },
                    'humidity':  {
                        "max": {
                            "value": list_max_humidity.maximum_relative,
                            "date": f'{moment_max_humidity.date} {moment_max_humidity.time}'
                        },
                        "min": {
                            "value": list_min_humidity.minimum_relative,
                            "date": f'{moment_min_humidity.date} {moment_min_humidity.time}'
                        }
                    }
                }
            }

            listresponse.append(details)

        MinMax.objects.get_or_create(
            type=agrupamento, station=listresponse[0])[0]

        return Response(listresponse)


class MinandMaxHumidity(APIView):
    def get(self, request, state: str):

        is_valid = get_object_or_404(State,
                                     name=state)

        humidity = Humidity.objects.filter(
            station__state__name=state).exclude(maximum_relative=None).prefetch_related('station', 'moment')

        humidity_month = humidity.annotate(
            month=TruncMonth('moment__date')).values('month').annotate(average_max_humidity=Max('maximum_relative'), average_min_humidity=Min('minimum_relative')).order_by('-month')

        response_list = []

        for month in humidity_month:
            respose = {}
            respose['month'] = month['month'].strftime("%m")
            respose['average_max_humidity'] = month['average_max_humidity']
            respose['average_min_humidity'] = month['average_min_humidity']
            response_list.append(respose)

        respose = {state: response_list}

        return Response(respose)


class MinandMaxHumidityAll(APIView):

    @method_decorator(cache_page(10))
    def get(self, request):

        all_states = State.objects.all()

        response_data = []

        for state in all_states:

            humidity = Humidity.objects.filter(
                station__state__name=state.name).exclude(maximum_relative=None).prefetch_related('station', 'moment')

            humidity_month = humidity.annotate(
                month=TruncMonth('moment__date')).values('month').annotate(average_max_humidity=Max('maximum_relative'), average_min_humidity=Min('minimum_relative')).order_by('-month')

            response_list = []

            for month in humidity_month:
                respose = {}
                respose['month'] = month['month'].strftime("%m")
                respose['average_max_humidity'] = month['average_max_humidity']
                respose['average_min_humidity'] = month['average_min_humidity']
                response_list.append(respose)

            respose = {state.name: response_list}
            response_data.append(respose)

        return Response(response_data)
