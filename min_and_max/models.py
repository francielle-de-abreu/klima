from django.db import models


class MinMax(models.Model):
    station = models.JSONField(null=True)
    type = models.CharField(max_length=255)
