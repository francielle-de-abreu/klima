from .views import MinMaxView, MinandMaxHumidity, MinandMaxHumidityAll
from django.urls import path

urlpatterns = [
    path('min-max/<str:agrupamento>', MinMaxView.as_view()),
    path('max_min_humidity_months/<str:state>/',
         MinandMaxHumidity.as_view()),
    path('max_min_humidity_months/',
         MinandMaxHumidityAll.as_view()),

]
