from .views import RegionView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'region', RegionView)
urlpatterns = router.urls
