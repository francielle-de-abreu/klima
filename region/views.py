from django.shortcuts import render
from rest_framework.mixins import ListModelMixin, CreateModelMixin,  RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin
from rest_framework.viewsets import GenericViewSet
from .models import Region
from .serializers import RegionSerializer


class RegionView(GenericViewSet,
                 RetrieveModelMixin,
                 CreateModelMixin,
                 UpdateModelMixin,
                 DestroyModelMixin,
                 ListModelMixin):

    queryset = Region.objects.all()
    serializer_class = RegionSerializer
