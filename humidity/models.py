from django.db import models
from station.models import Station
from moment.models import Moment


class Humidity(models.Model):
    station = models.ForeignKey(
        Station, on_delete=models.CASCADE, related_name='humidity')
    moment = models.ForeignKey(
        Moment, on_delete=models.CASCADE, related_name='humidity')
    maximum_relative = models.FloatField(null=True)
    minimum_relative = models.FloatField(null=True)
    relative = models.FloatField(null=True)

    def __str__(self):
        return f'{self.maximum_relative}'
