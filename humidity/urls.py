from .views import HumidityView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'humidity', HumidityView)
urlpatterns = router.urls
