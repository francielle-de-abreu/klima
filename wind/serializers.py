from .models import WindMeasurement
from rest_framework import serializers


class WindMeasurementSerializer(serializers.ModelSerializer):
    class Meta:
        model = WindMeasurement
        fields = '__all__'
