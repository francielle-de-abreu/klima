from .views import WindMeasurementView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'wind', WindMeasurementView)
urlpatterns = router.urls
