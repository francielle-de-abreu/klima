from django.db import models
from station.models import Station
from moment.models import Moment


class WindMeasurement(models.Model):
    station = models.ForeignKey(
        Station, on_delete=models.CASCADE, related_name='wind')
    moment = models.ForeignKey(
        Moment, on_delete=models.CASCADE, related_name='wind')
    direction = models.FloatField(null=True)
    gust = models.FloatField(null=True)
    speed = models.FloatField(null=True)

    def __str__(self):
        return f'{self.direction} - {self.gust}'
