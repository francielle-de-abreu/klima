# Generated by Django 3.1.7 on 2021-03-21 21:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wind', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='windmeasurement',
            old_name='wind_direction',
            new_name='direction',
        ),
        migrations.RenameField(
            model_name='windmeasurement',
            old_name='wind_gust',
            new_name='gust',
        ),
        migrations.RenameField(
            model_name='windmeasurement',
            old_name='moment_id',
            new_name='moment',
        ),
        migrations.RenameField(
            model_name='windmeasurement',
            old_name='wind_speed',
            new_name='speed',
        ),
        migrations.RenameField(
            model_name='windmeasurement',
            old_name='station_id',
            new_name='station',
        ),
    ]
